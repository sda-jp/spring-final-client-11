package pl.sda.jp.eventusclient11;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class EventRestClientService {

    private RestTemplate restTemplate;

    @Autowired
    public EventRestClientService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public List<ApiEventsDto> getEvents(){
        ApiEventsDto[] eventsDtos = restTemplate.getForObject("http://localhost:8080/api/events", ApiEventsDto[].class);
        return Arrays.asList(eventsDtos);
    }

}
