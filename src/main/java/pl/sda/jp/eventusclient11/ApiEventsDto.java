package pl.sda.jp.eventusclient11;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ApiEventsDto {
    private Long id;
    private String name;
    private Date startDate;
}
