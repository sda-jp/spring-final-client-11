package pl.sda.jp.eventusclient11;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    private EventRestClientService eventRestClientService;

    @Autowired
    public MainController(EventRestClientService eventRestClientService) {
        this.eventRestClientService = eventRestClientService;
    }

    @GetMapping("/")
    public String showMainPage(Model model){
        model.addAttribute("events", eventRestClientService.getEvents());
        return "index";
    }
}
