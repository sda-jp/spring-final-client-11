package pl.sda.jp.eventusclient11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventusClient11Application {

    public static void main(String[] args) {
        SpringApplication.run(EventusClient11Application.class, args);
    }
}
